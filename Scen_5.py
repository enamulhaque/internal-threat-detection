import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


lgon = pd.read_csv(r"C:\Users\Admin_2\Downloads\Data Sets\PROCESS\logon2010.csv", parse_dates=['date'])



lgon.month.hist(bins=12)


mdf = lgon.month.value_counts()
mdf = mdf.to_frame().sort_index()
mdf['avglast2'] = (mdf.month.shift(1) + mdf.month.shift(2))/2
mdf['cm'] = mdf.month < mdf.avglast2
affmon = mdf[mdf.cm].index.tolist()



reader = pd.read_csv(r"C:\Users\Admin_2\Downloads\Data Sets\PROCESS\web.csv", parse_dates=['date'], chunksize=100000)



for chunk in reader:
    df = chunk[ (chunk.date.dt.year == 2010) & (chunk.date.dt.month.isin(affmon)) & (chunk['activity'] == "WWW Upload") & (chunk['url'].str.contains("dropbox")) ]
    with open(r"C:\Users\Admin_2\Downloads\Data Sets\PROCESS\Scen_5.csv", 'a') as f:
        df.to_csv(f, mode='a', header=False)



scn = pd.read_csv(r"C:\Users\Admin_2\Downloads\Data Sets\PROCESS\Scen_5.csv", parse_dates=['date'])

x = scn[scn.content.str.contains("job|career")]

scn['month'] = scn.date.dt.month

scn.to_csv(r"C:\Users\Admin_2\Downloads\Data Sets\PROCESS\Scen_5_m.csv")



df = pd.read_csv(r"Scen_5_m.csv", parse_dates=['date'])
x = df.groupby(['user','month']).activity.count()
xdic = x.to_dict()
un = df.user.unique()

def outlierlist(dic, usernames, thresh=3.31):
    outliers = []
    lst = []
    sd = []
    for un in usernames:
        for m in range(1,13):
            lst.append(dic[(un,m)])            
        sd = np.abs(lst - np.mean(lst)) / np.std(lst)
        for i in sd:
            if i > thresh:
                outliers.append((un, list(sd).index(i)+1, i))
        lst[:] = []
    return outliers



x = outlierlist(xdic, un)


